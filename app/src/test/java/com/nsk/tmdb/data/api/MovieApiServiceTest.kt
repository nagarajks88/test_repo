package com.nsk.tmdb.data.api

import com.google.common.truth.Truth
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MovieApiServiceTest {

    private lateinit var service: MoviesApiService
    private lateinit var server: MockWebServer

    @Before
    fun setUp() {
        server = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(server.url(""))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(MoviesApiService::class.java)
    }

    private fun enqueueMockResponse(
        fileName: String
    ){
        val inputStream = javaClass.classLoader!!.getResourceAsStream(fileName)
        val source = inputStream.source().buffer()
        val mockResponse = MockResponse()
        mockResponse.setBody(source.readString(Charsets.UTF_8))
        server.enqueue(mockResponse)
    }

    @Test
    fun getPopularMovies_sentRequest_receivedExpected(){
        runBlocking {
            enqueueMockResponse("popular_movies.json")
            val responseBody = service.getPopularMovies("en-US",1).body()
            val request = server.takeRequest()
            Truth.assertThat(responseBody).isNotNull()
            Truth.assertThat(request.path).isEqualTo("/movie/popular?language=en-US&page=1&api_key=f1670663f4833630c036230ca3d2abf6")
        }
    }

    @Test
    fun getPopularMovies_receivedResponse_correctPageSize(){
        runBlocking {
            enqueueMockResponse("popular_movies.json")
            val responseBody = service.getPopularMovies("en-US",1).body()
            val resultList = responseBody?.results
            Truth.assertThat(resultList?.size).isEqualTo(20)
        }
    }

    @Test
    fun getPopularMovies_receivedResponse_getCorrectContent(){
        runBlocking {
            enqueueMockResponse("popular_movies.json")
            val responseBody = service.getPopularMovies("en-US",1).body()
            val resultList = responseBody?.results
            val movie = resultList?.get(0)
            Truth.assertThat(movie?.title).isEqualTo("Those Who Wish Me Dead")
            Truth.assertThat(movie?.posterPath).isEqualTo("/xCEg6KowNISWvMh8GvPSxtdf9TO.jpg")

        }
    }

    @After
    fun tearDown() {
        server.shutdown()
    }
}