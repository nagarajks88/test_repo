package com.nsk.tmdb.data.repository

import com.nsk.tmdb.data.model.MovieDetails
import com.nsk.tmdb.data.repository.datasource.MovieDetailsRemoteDataSource
import com.nsk.tmdb.data.util.Resource
import com.nsk.tmdb.domain.repository.MovieDetailRepository
import retrofit2.Response

class MovieDetailsRepositoryImpl(
    private val movieDetailsRemoteDataSource: MovieDetailsRemoteDataSource
) : MovieDetailRepository{

    override suspend fun getMovieDetails(movieId: Int, language: String): Resource<MovieDetails> {
        return responseToResource(movieDetailsRemoteDataSource.getMovieDetails(movieId,language))
    }

    private fun responseToResource(response: Response<MovieDetails>): Resource<MovieDetails>{
        if(response.isSuccessful){
            response.body()?.let { result ->
                return Resource.Success(result)
            }
        }
        return Resource.Error(response.message())
    }
}