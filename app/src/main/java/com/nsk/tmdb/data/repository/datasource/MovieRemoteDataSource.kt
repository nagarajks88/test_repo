package com.nsk.tmdb.data.repository.datasource

import com.nsk.tmdb.data.model.MovieDetails
import com.nsk.tmdb.data.model.PopularMovies
import retrofit2.Response

interface MovieRemoteDataSource {

    suspend fun getPopularMovies(language:String,page:Int): Response<PopularMovies>

}