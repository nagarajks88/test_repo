package com.nsk.tmdb.data.repository.datasourceimpl

import com.nsk.tmdb.data.api.MoviesApiService
import com.nsk.tmdb.data.model.MovieDetails
import com.nsk.tmdb.data.repository.datasource.MovieDetailsRemoteDataSource
import retrofit2.Response

class MovieDetailsRemoteDataSourceImpl(
    private val apiService: MoviesApiService
) : MovieDetailsRemoteDataSource {
    override suspend fun getMovieDetails(movieId: Int, language: String): Response<MovieDetails> {
        return apiService.getMovieDetails(movieId,language)
    }

}