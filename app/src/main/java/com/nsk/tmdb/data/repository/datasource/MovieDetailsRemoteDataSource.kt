package com.nsk.tmdb.data.repository.datasource

import com.nsk.tmdb.data.model.MovieDetails
import retrofit2.Response

interface MovieDetailsRemoteDataSource {
    suspend fun getMovieDetails(movieId:Int,language:String) : Response<MovieDetails>
}