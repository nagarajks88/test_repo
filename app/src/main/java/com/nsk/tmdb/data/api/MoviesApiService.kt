package com.nsk.tmdb.data.api

import com.nsk.tmdb.BuildConfig
import com.nsk.tmdb.data.model.MovieDetails
import com.nsk.tmdb.data.model.PopularMovies
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesApiService {

    @GET("movie/popular")
    suspend fun getPopularMovies(
        @Query("language")
        language: String,
        @Query("page")
        page:Int,
        @Query("api_key")
        api_key: String = BuildConfig.api_key
    ) : Response<PopularMovies>

    @GET("movie/{movie_id}")
    suspend fun getMovieDetails(
        @Path("movie_id")
        movieId: Int,
        @Query("language")
        language: String,
        @Query("api_key")
        api_key: String = BuildConfig.api_key,
    ) : Response<MovieDetails>
}