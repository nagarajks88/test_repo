package com.nsk.tmdb.data.repository

import com.nsk.tmdb.data.model.MovieDetails
import com.nsk.tmdb.data.model.PopularMovies
import com.nsk.tmdb.data.repository.datasource.MovieRemoteDataSource
import com.nsk.tmdb.data.util.Resource
import com.nsk.tmdb.domain.repository.MovieRepository
import retrofit2.Response

class MovieRepositoryImpl(
    private val movieRemoteDataSource: MovieRemoteDataSource
) : MovieRepository {
    override suspend fun getPopularMovies(language: String, page: Int): Resource<PopularMovies> {
        return responseToResource(movieRemoteDataSource.getPopularMovies(language,page))
    }

    private fun responseToResource(response: Response<PopularMovies>): Resource<PopularMovies>{
        if(response.isSuccessful){
            response.body()?.let { result ->
                return Resource.Success(result)
            }
        }
        return Resource.Error(response.message())
    }


}