package com.nsk.tmdb.data.repository.datasourceimpl

import com.nsk.tmdb.data.api.MoviesApiService
import com.nsk.tmdb.data.model.MovieDetails
import com.nsk.tmdb.data.model.PopularMovies
import com.nsk.tmdb.data.repository.datasource.MovieRemoteDataSource
import retrofit2.Response

class MovieRemoteDataSourceImpl(
        private val moviesApiService: MoviesApiService
    ) : MovieRemoteDataSource {

    override suspend fun getPopularMovies(language: String, page: Int): Response<PopularMovies> {
        return moviesApiService.getPopularMovies(language,page)
    }

}