package com.nsk.tmdb.presentation.di

import com.nsk.tmdb.domain.repository.MovieDetailRepository
import com.nsk.tmdb.domain.repository.MovieRepository
import com.nsk.tmdb.domain.usecase.GetMovieDetailsUseCase
import com.nsk.tmdb.domain.usecase.GetPopularMoviesUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Provides
    @Singleton
    fun providesGetPopularMoviesUseCase(movieRepository: MovieRepository) : GetPopularMoviesUseCase {
        return GetPopularMoviesUseCase(movieRepository)
    }

    @Provides
    @Singleton
    fun providesGetMovieDetailsUseCase(movieDetailRepository: MovieDetailRepository) : GetMovieDetailsUseCase {
        return GetMovieDetailsUseCase(movieDetailRepository)
    }
}