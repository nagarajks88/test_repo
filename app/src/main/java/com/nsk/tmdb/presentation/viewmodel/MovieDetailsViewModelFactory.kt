package com.nsk.tmdb.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nsk.tmdb.domain.usecase.GetMovieDetailsUseCase

class MovieDetailsViewModelFactory(
    private val app: Application,
    private val getMovieDetailsUseCase: GetMovieDetailsUseCase
) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MovieDetailsViewModel(app,getMovieDetailsUseCase) as T
    }
}