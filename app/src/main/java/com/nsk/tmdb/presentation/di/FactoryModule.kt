package com.nsk.tmdb.presentation.di

import android.app.Application
import com.nsk.tmdb.domain.usecase.GetMovieDetailsUseCase
import com.nsk.tmdb.domain.usecase.GetPopularMoviesUseCase
import com.nsk.tmdb.presentation.viewmodel.MovieDetailsViewModelFactory
import com.nsk.tmdb.presentation.viewmodel.PopularMoviesViewModelFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class FactoryModule {

    @Singleton
    @Provides
    fun providesPopularMoviesViewModel(application: Application,
        getPopularMoviesUseCase: GetPopularMoviesUseCase) : PopularMoviesViewModelFactory {
        return PopularMoviesViewModelFactory(application,getPopularMoviesUseCase)
    }

    @Singleton
    @Provides
    fun providesMovieDetailsViewModel(app: Application,
        getMovieDetailsUseCase: GetMovieDetailsUseCase) : MovieDetailsViewModelFactory {
        return MovieDetailsViewModelFactory(app,getMovieDetailsUseCase)
    }
}