package com.nsk.tmdb.presentation

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nsk.tmdb.R
import com.nsk.tmdb.data.model.MovieDetails
import com.nsk.tmdb.data.util.Resource
import com.nsk.tmdb.databinding.ActivityMovieDetailBinding
import com.nsk.tmdb.presentation.viewmodel.MovieDetailsViewModel
import com.nsk.tmdb.presentation.viewmodel.MovieDetailsViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MovieDetailActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: MovieDetailsViewModelFactory
    private lateinit var binding: ActivityMovieDetailBinding
    private lateinit var viewModel: MovieDetailsViewModel

    private var movieId: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =  ActivityMovieDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this,viewModelFactory).get(MovieDetailsViewModel::class.java)

        movieId = intent.getIntExtra("movie_id",-1)

        populateMovieDetails()
    }

    private fun populateMovieDetails() {

        viewModel.getMovieDetails("en-US",movieId)
        viewModel.movieDetails.observe(this, { response ->
            when(response) {
                is Resource.Success -> {
                    hideProgressBar()
                    response.data?.let { it ->
                        showMovieDetail(it)
                    }
                }

                is Resource.Loading -> {
                    showProgressBar()
                }

                is Resource.Error -> {
                    hideProgressBar()
                    response.message?.let {
                        Toast.makeText(this,"An error occurred $it", Toast.LENGTH_LONG).show()
                    }
                }
            }
        })
    }

    private fun showMovieDetail(it: MovieDetails) {
        binding.movieNameTv.text = it.title
        binding.overviewTv.text = it.overview
        binding.releaseDateTv.text = "${getString(R.string.release_date)} ${it.releaseDate}"

        val posterURL = "https://image.tmdb.org/t/p/w500"+it.posterPath
        Glide.with(binding.imageView.context)
            .load(posterURL)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(binding.imageView)

    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
        binding.group.visibility = View.VISIBLE
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
        binding.group.visibility = View.GONE
    }
}