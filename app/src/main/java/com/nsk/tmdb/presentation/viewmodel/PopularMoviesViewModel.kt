package com.nsk.tmdb.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.nsk.tmdb.R
import com.nsk.tmdb.data.model.PopularMovies
import com.nsk.tmdb.data.util.Resource
import com.nsk.tmdb.domain.usecase.GetPopularMoviesUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PopularMoviesViewModel(
    private val app: Application,
    private val getPopularMoviesUseCase: GetPopularMoviesUseCase
) : TMDBViewModel(app) {

    val popularMovies :MutableLiveData<Resource<PopularMovies>> = MutableLiveData()

    fun getPopularMovies(language: String, page: Int) = viewModelScope.launch(Dispatchers.IO) {
        popularMovies.postValue(Resource.Loading())

        try {

            if(isNetworkAvailable(app)) {
                val apiResult = getPopularMoviesUseCase.execute(language,page)
                popularMovies.postValue(apiResult)
            }else{
                popularMovies.postValue(Resource.Error(app.getString(R.string.internet_not_availble)))
            }

        } catch (ex: Exception){
            popularMovies.postValue(Resource.Error(ex.message.toString()))
        }

    }
}
