package com.nsk.tmdb.presentation.di

import com.nsk.tmdb.BuildConfig
import com.nsk.tmdb.data.api.MoviesApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetModule {

    @Singleton
    @Provides
    fun providesRetrofitInstance() : Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.base_url)
            .build()
    }

    @Singleton
    @Provides
    fun providesMoviesApiService(retrofit: Retrofit) : MoviesApiService {
        return retrofit.create(MoviesApiService::class.java)
    }

}