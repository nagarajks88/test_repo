package com.nsk.tmdb.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nsk.tmdb.domain.usecase.GetPopularMoviesUseCase

class PopularMoviesViewModelFactory(
    private val app: Application,
    private val getPopularMoviesUseCase: GetPopularMoviesUseCase
) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PopularMoviesViewModel(app,getPopularMoviesUseCase) as T
    }

}