package com.nsk.tmdb.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.nsk.tmdb.R
import com.nsk.tmdb.data.model.MovieDetails
import com.nsk.tmdb.data.util.Resource
import com.nsk.tmdb.domain.usecase.GetMovieDetailsUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

class MovieDetailsViewModel(
    private val app: Application,
    private val getMovieDetailsUseCase: GetMovieDetailsUseCase
) : TMDBViewModel(app) {

    val movieDetails : MutableLiveData<Resource<MovieDetails>> = MutableLiveData()

    fun getMovieDetails(language: String, movieId: Int) = viewModelScope.launch(Dispatchers.IO) {

        movieDetails.postValue(Resource.Loading())

        try {
            if(isNetworkAvailable(app)) {
                val apiResult = getMovieDetailsUseCase.execute(movieId,language)
                movieDetails.postValue(apiResult)
            }else{
                movieDetails.postValue(Resource.Error(app.getString(R.string.internet_not_availble)))
            }
        } catch (e: Exception){
            movieDetails.postValue(Resource.Error(e.message.toString()))
        }
    }
}