package com.nsk.tmdb.presentation.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nsk.tmdb.data.model.Result
import com.nsk.tmdb.databinding.PopularItemLayoutBinding
import com.nsk.tmdb.presentation.MovieDetailActivity

class PopularMoviesAdapter : RecyclerView.Adapter<PopularMoviesAdapter.ViewHolder>() {


    private val movieList =ArrayList<Result>()

    private val temp = ArrayList<Result>()

    fun setList(movies: List<Result>,isSearch: Boolean){
        if(isSearch){
            temp.addAll(movieList)
            movieList.clear()
        }

        movieList.addAll(movies)
    }

    fun clearTempList(){
        movieList.clear()
        if(temp.size > 0)
            movieList.addAll(temp)
        temp.clear()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = PopularItemLayoutBinding.inflate(LayoutInflater.from(parent.context),
                        parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = movieList[position]
        holder.bind(result)
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    inner class ViewHolder(
        val binding: PopularItemLayoutBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(result: Result){
            binding.movieNameTv.text =result.title
            val posterURL = "https://image.tmdb.org/t/p/w500"+result.posterPath
            Glide.with(binding.imageView.context)
                .load(posterURL)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.imageView)

            binding.root.setOnClickListener {
                val intent = Intent(binding.root.context, MovieDetailActivity::class.java)
                intent.putExtra("movie_id",result.id)
                binding.root.context.startActivity(intent)
            }
        }
    }
}