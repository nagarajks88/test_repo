package com.nsk.tmdb.presentation.di

import com.nsk.tmdb.presentation.adapter.PopularMoviesAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AdapterModule {

    @Singleton
    @Provides
    fun providesPopularMoviesAdapter() : PopularMoviesAdapter {
        return PopularMoviesAdapter()
    }
}