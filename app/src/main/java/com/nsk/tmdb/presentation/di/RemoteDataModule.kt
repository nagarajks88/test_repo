package com.nsk.tmdb.presentation.di

import com.nsk.tmdb.data.api.MoviesApiService
import com.nsk.tmdb.data.repository.datasource.MovieDetailsRemoteDataSource
import com.nsk.tmdb.data.repository.datasource.MovieRemoteDataSource
import com.nsk.tmdb.data.repository.datasourceimpl.MovieDetailsRemoteDataSourceImpl
import com.nsk.tmdb.data.repository.datasourceimpl.MovieRemoteDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RemoteDataModule {

    @Singleton
    @Provides
    fun providesRemoteDataSource(moviesApiService: MoviesApiService) : MovieRemoteDataSource {
        return MovieRemoteDataSourceImpl(moviesApiService)
    }

    @Singleton
    @Provides
    fun provideMovieDetailRemoteSource(moviesApiService: MoviesApiService) : MovieDetailsRemoteDataSource {
        return MovieDetailsRemoteDataSourceImpl(moviesApiService)
    }
}