package com.nsk.tmdb.presentation.di

import com.nsk.tmdb.data.repository.MovieDetailsRepositoryImpl
import com.nsk.tmdb.data.repository.MovieRepositoryImpl
import com.nsk.tmdb.data.repository.datasource.MovieDetailsRemoteDataSource
import com.nsk.tmdb.data.repository.datasource.MovieRemoteDataSource
import com.nsk.tmdb.domain.repository.MovieDetailRepository
import com.nsk.tmdb.domain.repository.MovieRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun providesMoviesRepository(movieRemoteDataSource: MovieRemoteDataSource) : MovieRepository {
        return MovieRepositoryImpl(movieRemoteDataSource)
    }


    @Singleton
    @Provides
    fun providesMovieDetailsRepository(movieDetailsRemoteDataSource: MovieDetailsRemoteDataSource) : MovieDetailRepository {
        return MovieDetailsRepositoryImpl(movieDetailsRemoteDataSource)
    }
}