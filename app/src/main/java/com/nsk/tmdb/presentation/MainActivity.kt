package com.nsk.tmdb.presentation

import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.AbsListView
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nsk.tmdb.R
import com.nsk.tmdb.data.model.Result
import com.nsk.tmdb.data.util.Resource
import com.nsk.tmdb.databinding.ActivityMainBinding
import com.nsk.tmdb.presentation.adapter.PopularMoviesAdapter
import com.nsk.tmdb.presentation.viewmodel.PopularMoviesViewModel
import com.nsk.tmdb.presentation.viewmodel.PopularMoviesViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: PopularMoviesViewModelFactory

    @Inject
    lateinit var popularMoviesAdapter: PopularMoviesAdapter
    lateinit var viewModel: PopularMoviesViewModel
    private lateinit var binding: ActivityMainBinding

    private var language: String = "en-US"
    private var page = 1
    private var isScrolling = false
    private var isLoading = false
    private var isLastPage = false
    private var pages = 0
    private var movies = ArrayList<Result>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = "TMDB - Popular Movies"
        viewModel = ViewModelProvider(this, factory).get(PopularMoviesViewModel::class.java)
        initRecyclerView()
        viewPopularMoviesList()
    }

    private fun viewPopularMoviesList() {
        viewModel.getPopularMovies(language, page)
        viewModel.popularMovies.observe(this, { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    response.data?.let { it ->
                        movies.addAll(it.results)
                        popularMoviesAdapter.setList(movies,false)
                        if (it.totalResults % 20 == 0) {
                            pages = it.totalResults / 20
                        } else {
                            pages = it.totalResults / 20 + 1
                        }
                        isLastPage = page == pages
                    }
                    showRecyclerView()
                }

                is Resource.Loading -> {
                    hideRecyclerView()
                    showProgressBar()
                }

                is Resource.Error -> {
                    hideProgressBar()
                    hideRecyclerView()
                    response.message?.let {
                        Toast.makeText(this, "An error occurred $it", Toast.LENGTH_LONG).show()
                    }
                }
            }
        })
    }

    private fun initRecyclerView() {
        binding.recyclerView.apply {
            adapter = popularMoviesAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            addOnScrollListener(onScrollListener)
        }
    }

    private fun showProgressBar() {
        isLoading = true
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        isLoading = false
        binding.progressBar.visibility = View.GONE
    }

    private fun showRecyclerView() {
        binding.recyclerView.visibility = View.VISIBLE
    }

    private fun hideRecyclerView() {
        binding.recyclerView.visibility = View.GONE
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.app_menu, menu)
        val search = menu?.findItem(R.id.app_bar_search)
        val searchView = search?.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if(newText?.length!! > 2) {
                    val searchList =
                        movies.filter { it.title.lowercase().contains(newText.toString()) }
                    popularMoviesAdapter.setList(searchList, true)
                    popularMoviesAdapter.notifyDataSetChanged()
                }
                return true
            }
        })

        searchView.setOnCloseListener {
            popularMoviesAdapter.clearTempList()
            popularMoviesAdapter.notifyDataSetChanged()
            return@setOnCloseListener false
        }

        return true
    }

    private val onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                isScrolling = true
            }

        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val layoutManager = binding.recyclerView.layoutManager as LinearLayoutManager
            val sizeOfTheCurrentList = layoutManager.itemCount
            val visibleItems = layoutManager.childCount
            val topPosition = layoutManager.findFirstVisibleItemPosition()

            val hasReachedToEnd = topPosition + visibleItems >= sizeOfTheCurrentList
            val shouldPaginate = !isLoading && !isLastPage && hasReachedToEnd && isScrolling
            if (shouldPaginate) {
                page++
                viewModel.getPopularMovies(language, page)
                isScrolling = false
            }


        }
    }

}