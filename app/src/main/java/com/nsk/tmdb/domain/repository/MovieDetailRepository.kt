package com.nsk.tmdb.domain.repository

import com.nsk.tmdb.data.model.MovieDetails
import com.nsk.tmdb.data.util.Resource

interface MovieDetailRepository {
    suspend fun getMovieDetails(movieId: Int, language: String) : Resource<MovieDetails>
}