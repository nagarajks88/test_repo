package com.nsk.tmdb.domain.usecase

import com.nsk.tmdb.data.model.PopularMovies
import com.nsk.tmdb.data.util.Resource
import com.nsk.tmdb.domain.repository.MovieRepository

class GetPopularMoviesUseCase(
    private val movieRepository: MovieRepository
) {
    suspend fun execute(language:String, page: Int) : Resource<PopularMovies> =
        movieRepository.getPopularMovies(language,page)
}