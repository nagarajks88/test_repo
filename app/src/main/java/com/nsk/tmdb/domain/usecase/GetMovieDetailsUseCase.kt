package com.nsk.tmdb.domain.usecase

import com.nsk.tmdb.data.model.MovieDetails
import com.nsk.tmdb.data.util.Resource
import com.nsk.tmdb.domain.repository.MovieDetailRepository

class GetMovieDetailsUseCase(
    private val movieDetailRepository: MovieDetailRepository
) {

    suspend fun execute(movieId: Int, language:String) : Resource<MovieDetails> =
        movieDetailRepository.getMovieDetails(movieId,language)
}