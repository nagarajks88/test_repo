package com.nsk.tmdb.domain.repository

import com.nsk.tmdb.data.model.PopularMovies
import com.nsk.tmdb.data.util.Resource

interface MovieRepository {
    suspend fun getPopularMovies(language:String,page:Int) : Resource<PopularMovies>
}